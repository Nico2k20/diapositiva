const urlsImgs = ["https://cdn.pixabay.com/photo/2021/10/05/14/32/ocean-6682870_960_720.jpg",
                "https://cdn.pixabay.com/photo/2019/12/19/03/05/christmas-4705290_960_720.jpg",
                "https://cdn.pixabay.com/photo/2021/11/13/19/27/architecture-6792169_960_720.jpg",
                "https://cdn.pixabay.com/photo/2021/01/29/11/33/game-5960731_960_720.jpg",
                "https://cdn.pixabay.com/photo/2020/03/25/16/55/spain-4967963_960_720.jpg",
                "https://cdn.pixabay.com/photo/2016/12/26/13/28/taxi-1932107_960_720.jpg"]

let indexUrls =0;
let tiempo = 1500;
let cambioAuto;
let cambioAutoActivado=false;


function cambiarImagenComienzo(){
    

    document.getElementById("container").style.backgroundImage = "url('"+urlsImgs[indexUrls]+"')";
    
}

function cambiarImagen(indice){
    indexUrls+= indice;

    if(indexUrls>urlsImgs.length-1||indexUrls<0)
        indexUrls=0;
    
    document.getElementById("container").style.transition = "all 0.5s";
    document.getElementById("container").style.backgroundImage = "url('"+urlsImgs[indexUrls]+"')";
}

function activarCambioAutomatico(){
    if(cambioAutoActivado)  return;
    
    cambioAutomatico();
}

function cambioAutomatico() {
    console.log(indexUrls);
    document.getElementById("container").style.transition = "all 0.5s";
    document.getElementById("container").style.backgroundImage = "url('"+urlsImgs[indexUrls]+"')";
    if(indexUrls<urlsImgs.length-1){
        indexUrls++;
    }
    else
        indexUrls=0;

    cambioAutoActivado=true;
    
    cambioAuto = setTimeout("cambioAutomatico()",tiempo);

}


function detenerCambioAutomatico (){
    cambioAutoActivado=false;

    clearTimeout(cambioAuto);
}


window.onload = cambiarImagenComienzo;